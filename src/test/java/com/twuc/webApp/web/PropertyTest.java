package com.twuc.webApp.web;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@AutoConfigureMockMvc
class PropertyTest {
    @Autowired
    MockMvc mockMvc;

//    @Autowired
//    private EmployeesRepository employeesRepository;

    @Test
    void should_return_property() throws Exception {
        MockHttpServletResponse response = mockMvc.perform(get("/api/classicmodels"))
                .andExpect(status().is(201))
                .andReturn().getResponse();
        assertEquals(0, response.getContentAsString().length());
    }



}
