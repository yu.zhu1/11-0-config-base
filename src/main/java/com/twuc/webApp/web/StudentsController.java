package com.twuc.webApp.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StudentsController {
    private final StudentsRepository studentsRepository;

    public StudentsController(StudentsRepository studentsRepository) {
        this.studentsRepository = studentsRepository;
    }

    @GetMapping("/api/classicmodels")
    ResponseEntity getStudents() {
//        studentsRepository.save(new Students(1,"laji"));
        Students student = studentsRepository.findById(1).orElse(null);
        return ResponseEntity.status(201).body(student);
    }

}
