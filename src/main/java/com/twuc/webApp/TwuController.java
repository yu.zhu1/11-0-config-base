package com.twuc.webApp;

import com.twuc.webApp.Property.TwuConfigerationProperty;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TwuController {

//    @Value("${twuc.webapp.key}")
//    private String key;

//    @Autowired
//    Environment environment;

    private TwuConfigerationProperty twuConfigerationProperty;

    public TwuController(TwuConfigerationProperty twuConfigerationProperty) {
        this.twuConfigerationProperty = twuConfigerationProperty;
    }

    @GetMapping("/api/twu")
    String getProperty() {
        return twuConfigerationProperty.getKey();
//                environment.getProperty("twuc.webapp.key");
    }

}
